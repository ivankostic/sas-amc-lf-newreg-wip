module.exports = {
  "root": true,
  "extends": [
    "eslint:recommended",
  ],
  "globals": {
    "wp": true
  },
  "env": {
    "node": true,
    "es6": true,
    "amd": true,
    "browser": true,
    "jquery": true
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaFeatures": {
      "globalReturn": true,
      "objectLiteralDuplicateProperties": false,
      "ecmaVersion": true,
      "jsx": false
    },
    "ecmaVersion": 2017,
    "sourceType": "module",
    "allowImportExportEverywhere": true
  },
  "plugins": [
    "import"
  ],
  "settings": {
    "import/core-modules": [],
    "import/ignore": [
      "node_modules",
      "\\.(coffee|scss|css|less|hbs|svg|json)$"
    ]
  },
  "rules": {
    'no-unused-vars': ['error', { "args": "none" }],
    "no-console": 0,
    "comma-dangle": [
      "error",
      {
        "arrays": "always-multiline",
        "objects": "always-multiline",
        "imports": "always-multiline",
        "exports": "always-multiline",
        "functions": "ignore"
      }
    ]
  }
}
